﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AftaScool.Models.Behaviour
{
    public class BehaviourModel
    {
        public virtual long? Id { get; set; }

        [MaxLength(20)]
        [Required]
        public virtual string Type { get; set; }

    }
}