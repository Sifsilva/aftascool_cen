﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AftaScool.Models.AssessorSchool
{
    public class AssessorSchoolGridModel
    {
        public long? Id { get; set; }

        public long AssessorId { get; set; }

       
        public long SchoolId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}