﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AftaScool.Models
{
    public class EmployeeModel
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
    }
}
