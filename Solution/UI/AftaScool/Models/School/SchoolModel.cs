﻿using AftaScool.BL.Entities.SchoolData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;

namespace AftaScool.Models.School
{
    public class SchoolModel
    {
        public long? Id { get; set; }

        [Required]
        public virtual string SchoolName { get; set; }
        [Required]
        public SchoolType SchoolType { get; set; }
        [Required]
        public virtual string AddressLine1 { get; set; }


        public virtual string AddressLine2 { get; set; }

        [Required]
        public virtual string City { get; set; }
        [Required]
        public virtual string PostalCode { get; set; }

        public virtual string telephone { get; set; }

        public virtual DbGeography Location { get; set; }
    }
}