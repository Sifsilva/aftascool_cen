﻿using AftaScool.BL.Entities.SchoolData;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;
using TCR.Lib.Utility;

namespace AftaScool.Models.School
{
    public class SchoolGridModel
    {
        public long? Id { get; set; }


        public virtual string SchoolName { get; set; }
        public SchoolType SchoolType { get; set; }

        public string Schoool { get { return NameSplitting.SplitCamelCase(SchoolType); } }
        public virtual string AddressLine1 { get; set; }


        public virtual string AddressLine2 { get; set; }


        public virtual string City { get; set; }


        public virtual string PostalCode { get; set; }

        public virtual string Telephone { get; set; }

        public virtual DbGeography Location { get; set; }
    }
}