﻿using AftaScool.BL.Entities.SecurityData;
using AftaScool.BL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TCR.Lib.Utility;

namespace AftaScool.Models.Learner
{
    public class LearnerGridModel
    {
        public virtual long Id { get; set; }

      
        public  string LearnerName { get; set; }

        public string LearnerSurname { get; set; }

        public string Grade { get; set; }

        public  GenderType GenderType { get; set; }

        public string Gender { get { return NameSplitting.SplitCamelCase(GenderType); } }

        public StatusType StatusType { get; set; }

        public virtual string Status { get { return NameSplitting.SplitCamelCase(StatusType); } }

        public virtual string IdPassportNum { get; set; }

        public virtual string Telephone { get; set; }

        public virtual string AddressLine1 { get; set; }

        public virtual string AddressLine2 { get; set; }

  
        public virtual string City { get; set; }


        public virtual string PostalCode { get; set; }

    }
}