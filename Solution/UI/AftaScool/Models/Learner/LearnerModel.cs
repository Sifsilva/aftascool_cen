﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using AftaScool.BL.Entities.SecurityData;
using System.ComponentModel.DataAnnotations.Schema;
using AftaScool.BL.Types;

namespace AftaScool.Models.Learner
{
    public class LearnerModel
    {
        public virtual long? Id { get; set; }

        [Required]
      
        public virtual string LearnerName { get; set; }

        public virtual string LearnerSurname { get; set; }

        public virtual string Grade { get; set; }

        public virtual GenderType Gender { get; set; }


        [Required]
      
        public virtual string IdPassportNum { get; set; }


        [Required]
        public virtual string Telephone { get; set; }


        [Required]
        public virtual string AddressLine1 { get; set; }

        [Required]
        public virtual string AddressLine2 { get; set; }

        [Required]
        public virtual string City { get; set; }

      [Required]
        public virtual string PostalCode { get; set; }

    }
}