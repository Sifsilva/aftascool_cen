﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AftaScool.Models.Learner
{
    public class LearnerAssessorModel
    {
        public virtual long Id { get; set; }

        
        public virtual long AssessorId { get; set; }

        public virtual long LearnerId { get; set; }
    }
}