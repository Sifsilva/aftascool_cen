﻿using AftaScool.BL.Entities.SchoolData;
using AftaScool.BL.Entities.LearnerData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AftaScool.Models.LearnerSchool
{
    public class LearnerSchoolModel
    {
        public virtual long? Id { get; set; }

        public virtual long LearnerId { get; set; }
       
        public virtual long SchoolId { get; set; }
        [Required]
        public virtual DateTime StartDate { get; set; }
        [Required]
        public virtual DateTime EndDate { get; set; }
    }
}