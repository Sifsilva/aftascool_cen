﻿using AftaScool.BL.Entities.SecurityData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AftaScool.Models.Assessor
{
    public class AssessorModel
    {
        public virtual long? Id { get; set; }
        public virtual long? UserIdentityId { get; set; }
        [ForeignKey("UserIdentityId")]

        [Required]
        [Display(Name = "Email")]
        public string UserName { get; set; }

        public GenderType GenderType { get; set; }

        [Required]
        [Display(Name = "IDOrPassport")]
        public string IDOrPassport { get; set; }

        [Required]
        [Display(Name = "Surname")]
        public string Surname { get; set; }

        [Required]
        [Display(Name = "FirstName")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Telephone")]
        public string Telephone { get; set; }


        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }


        public string PostalCode { get; set; }

        public string City { get; set; }

        public string AddressLine2 { get; set; }

        public string AddressLine1 { get; set; }

        public virtual UserIdentity UserIdentities { get; set; }
    }
}