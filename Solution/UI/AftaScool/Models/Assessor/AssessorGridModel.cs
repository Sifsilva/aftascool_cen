﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AftaScool.BL.Entities.SchoolData;
using AftaScool.BL.Entities.AssessorData;
using AftaScool.Models.Assessor;
using AftaScool.BL.Entities.SecurityData;
using TCR.Lib.Utility;

namespace AftaScool.Models
{
    public class AssessorGridModel
    {
        public virtual long? Id { get; set; }
        public virtual long? UserIdentityId { get; set; }

        public virtual string UserName { get; set; }

       
        public virtual string Email { get; set; }
      
        public virtual string Title { get; set; }
        
        public virtual string FirstName { get; set; }
       
        public virtual string Surname { get; set; }
        
        public virtual string idOrPassport { get; set; }

        public string Gender { get { return NameSplitting.SplitCamelCase(GenderType); } }
       
        public GenderType GenderType { get; set; }
        

        public virtual string Telephone { get; set; }
       
        public virtual string AddressLine1 { get; set; }
     
        public virtual string AddressLine2 { get; set; }
      
        public string City { get; set; }
       
        public virtual string PostalCode { get; set; }
        public virtual UserIdentity UserIdentities { get; set; }
    }
}