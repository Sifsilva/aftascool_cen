﻿using AftaScool.BL.Entities.QuestionnaireData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AftaScool.Models.BehaviourQuestion
{
    public class BehaviourQuestionModel
    {
       
        public virtual long? Id { get; set; }

        public virtual long BehaviourId { get; set; }
       

        public virtual double MinimumWeight { get; set; }

        public virtual double MaximumWeight { get; set; }


    }
}