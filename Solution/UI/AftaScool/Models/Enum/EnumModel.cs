﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AftaScool.Models.Enum
{
    public class EnumModel
    {
        public string Value { get; set; }

        public string Description { get; set; }
    }
}
