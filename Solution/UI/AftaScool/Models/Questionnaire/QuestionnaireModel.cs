﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AftaScool.BL.Types;

namespace AftaScool.Models.Questionnaire
{
    public class QuestionnaireModel
    {

        public virtual long? Id { get; set; }


        public virtual long? AssessorId { get; set; }


        public virtual long LearnerId { get; set; }

        [Required]
        public virtual DateTime QuestionnaireDate { get; set; }


    }
}