﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AftaScool.BL.Entities.Behaviour;
using AftaScool.BL.Entities.QuestionnaireData;
using AftaScool.Models.Questionnaire;
using System.ComponentModel.DataAnnotations;


namespace AftaScool.Models.QuestionnaireQuestion
{
    public class QuestionnaireQuestionModel
    {
        public virtual long? Id { get; set; }

        public virtual long QuestionnaireId { get; set; }
       

        public virtual long BehaviourQuestionId { get; set; }
        
        [Required]
        public virtual string Trait { get; set; }
    }
}