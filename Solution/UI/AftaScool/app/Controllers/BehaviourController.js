﻿AngularApp.controller('BehaviourController',
    ['$scope', '$location', '$window', '$routeParams', 'BehaviourService',
    function ($scope, $location, $window, $routeParams, BehaviourService) {

        $scope.behaviourModel = {
            id: null,
            type: ''

        };


        $scope.submitForm = function () {

            $scope.$broadcast('show-errors-check-validity');

            if ($scope.NewBehaviourForm.$invalid)
                return;


            var result = BehaviourService.SaveBehaviour($scope.behaviourModel.id,
                                                $scope.behaviourModel.type



                );


            result.then(function (result) {
                $location.path('/home');
            },
            function (error) {
                alert('Some error occured');
            });
        }

        $scope.resetForm = function () {
            $scope.$broadcast('show-errors-reset');
        }

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        }
    }
    ]);