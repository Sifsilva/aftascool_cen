﻿AngularApp.controller('LearnerController',
    ['$scope', '$location', '$window', '$routeParams', '$rootScope', 'LearnerService', 'EnumsService', 'LearnerSchoolService',

function ($scope, $location, $window, $routeParams,rootScope, LearnerService, EnumsService, LearnerSchoolService) {

        
        $scope.learnerModel = {

            id: null,
            learnerName: '',
            learnerSurname: '',
            grade: '',
            idorpassport:'',
            genderType: '',
            addressline1:'',
            addressline2:'',
            city: '',
            postalCode: '',
            telephone: ''
           

        }

       $scope.genderTypes = EnumsService.genderTypes();


        $scope._id = $scope.learnerModel.id
       $scope._id = $routeParams.id;

       if ($scope._id > 0) {
           LearnerService.LearnerGet($scope._id).then(
                function (result) {

                    $scope.learnerModel.id = result.data.id;
                    $scope.learnerModel.learnerName = result.data.learnerName;
                    $scope.learnerModel.learnerSurname = result.data.learnerSurname;
                    $scope.learnerModel.grade = result.data.grade;
                    $scope.learnerModel.idorpassport = result.data.idPassportNum;
                    $scope.learnerModel.genderType= result.data.gender;
                    $scope.learnerModel.addressline1 = result.data.addressLine1;
                    $scope.learnerModel.addressline2 = result.data.addressLine2;
                    $scope.learnerModel.city = result.data.city;
                    $scope.learnerModel.postalCode = result.data.postalCode;
                    $scope.learnerModel.telephone = result.data.telephone;
                },
          

                function (error) {
                    handleError(error);
                });
        };
       

        AngularApp.run(function ($rootScope) {

            $rootScope.searchQueryChanged = function (query) {
                $rootScope.searchQuery = query;
            }
        })

        $scope.submitForm = function () {

            $scope.$broadcast('show-errors-check-validity');

            if ($scope.SaveLearnerForm.$invalid)
                return;

            var result = LearnerService.LearnerSave($scope.learnerModel.id,$scope.learnerModel.learnerName,
                 $scope.learnerModel.learnerSurname,
                 $scope.learnerModel.grade, $scope.learnerModel.idorpassport, $scope.learnerModel.genderType,
                 $scope.learnerModel.addressline1, $scope.learnerModel.addressline2,
                 $scope.learnerModel.city, $scope.learnerModel.postalCode, $scope.learnerModel.telephone
            
                 );  
            result.then(function (result) {
                $location.path('/SaveLearnerSchool/' + $scope.learnerModel.idorpassport);
            },
            function (error) {
                alert('Some error occured');
            });
           // result.then(function (result) {
           //     $location.path('/LearnerMaintenance');
           // },
           //function (error) {
           //    alert('Some error occured');
           //});
        }

        $scope.resetForm = function () {
            $scope.$broadcast('show-errors-reset');
        }

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        }

        $scope.newLearner = function () {
            $location.path("/LearnerEdit/0");
        };

        var handleError = function (error) {
            $scope.hasError = true;
            $scope.errorMessage = error.statusText;
        }

        //Learner Grid
        $scope.sortKeyOrder = {
            key: 'learnerName',
            order: 'ASC',
        };

       
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.maxSize = 5;
        $scope.recordsPerPage = 20;
        $scope.numberOfPageButtons = 5;

        $scope.sort = function (col) {
            if ($scope.sortKeyOrder.key === col) {
                if ($scope.sortKeyOrder.order == 'ASC')
                    $scope.sortKeyOrder.order = 'DESC';
                else
                    $scope.sortKeyOrder.order = 'ASC';
            } else {
                $scope.sortKeyOrder.key = col;
                $scope.sortKeyOrder.order = 'ASC';
            }
            loadGrid();
        };


        var loadGrid = function () {
            var searchFor = '';

            LearnerService.LearnerGrid($scope.currentPage, $scope.recordsPerPage,
                                      $scope.sortKeyOrder.key, $scope.sortKeyOrder.order, searchFor).then(
                function (result) {
                    $scope.learnerGrid = result.data.results;
                    $scope.totalItems = result.data.recordCount;
                },
                function (error) {
                    alert("an error occured: unable to get data");
                });


        };


        $scope.pageChanged = function () {
            loadGrid();
        };


        loadGrid();
    }

    ]);
