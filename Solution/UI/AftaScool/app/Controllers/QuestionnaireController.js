﻿AngularApp.controller('QuestionnaireController',
    ['$scope', '$location', '$window', '$routeParams', 'QuestionnaireService', 'LearnerService', 'AssessorService',
    function ($scope, $location, $window, $routeParams, QuestionnaireService, LearnerService, AssessorService) {
        
        $scope.questionnaireModel = {
            id: null,
            assessorId: null,
            learnerId: '',
            questionnaireDate: ''

        };

        $scope.questionnaireModel.learnerId = $routeParams.id;

      
        $scope.submitForm = function () {

            $scope.$broadcast('show-errors-check-validity');

            if ($scope.NewQuestionnaireForm.$invalid)
                return;
        
            var result = QuestionnaireService.QuestionnaireSave($scope.questionnaireModel.id,
                                                $scope.questionnaireModel.assessorId,
                                                $scope.questionnaireModel.learnerId,
                                                $scope.questionnaireModel.questionnaireDate


                );


            result.then(function (result) {
                $location.path('/BehaviourQuestionSave/' + $scope.questionnaireModel.learnerId);
            },

            function (error) {
                alert('Some error occured');
            });
        }

        $scope.resetForm = function () {
            $scope.$broadcast('show-errors-reset');
        }

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        }
    }
    ]);