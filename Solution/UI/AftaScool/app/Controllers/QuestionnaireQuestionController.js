﻿AngularApp.controller('QuestionnaireQuestionController',
    ['$scope', '$location', '$window', '$routeParams', 'QuestionnaireQuestionService', 'BehaviourService',
function ($scope, $location, $window, $routeParams, QuestionnaireQuestionService, BehaviourService) {
    $scope.questionnaireQuestionModel = {
        id: null,
        questionnaireId: 2,
        behaviourQuestionId: '',
        trait: ''

    };

    $scope.questionnaireQuestionModel.id = $routeParams.id;

    if ($scope.questionnaireQuestionModel.id > 0) {
        QuestionnaireQuestionService.QuestionnaireQuestionGet($scope.questionnaireQuestionModel.id).then(
            function (result) {

                $scope.questionnaireQuestionModel.id = result.data.id;
                $scope.questionnaireQuestionModel.questionnaireId = result.data.questionnaireId;
                $scope.questionnaireQuestionModel.behaviourQuestionId = result.data.behaviourQuestionId;
                $scope.questionnaireQuestionModel.trait = result.data.trait;

            },

            function (error) {
                handleError(error);
            });
    };

    BehaviourService.BehaviourList().then(
  function (result) {
      $scope.behaviourList = result.data;
  },
  function (error) {
      $scope.hasError = true;
      $scope.errorMessage = error.statusText;
  });

    $scope.submitForm = function () {

        $scope.$broadcast('show-errors-check-validity');

        if ($scope.NewQuestionnaireQuestionForm.$invalid)
            return;

        var result = QuestionnaireQuestionService.QuestionnaireQuestionSave($scope.questionnaireQuestionModel.id,
                                            $scope.questionnaireQuestionModel.questionnaireId,
                                            $scope.questionnaireQuestionModel.behaviourQuestionId,
                                            $scope.questionnaireQuestionModel.trait


            );


        result.then(function (result) {
            $location.path('/home');
        },

         function (error) {
             handleError(error);
         });
    }

    $scope.resetForm = function () {
        $scope.$broadcast('show-errors-reset');
    }

    $scope.cancelForm = function () {
        $scope.$broadcast('show-errors-reset');
        $window.history.back();
    }

    $scope.NewQuestionnaireQuestionForm = function () {
        $location.path("/QuestionnaireQuestionEdit/0");
    };

    var handleError = function (error) {
        $scope.hasError = true;
        $scope.errorMessage = error.statusText;
    }


}

    ]);

