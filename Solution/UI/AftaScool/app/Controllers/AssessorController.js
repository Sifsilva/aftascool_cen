﻿AngularApp.controller('AssessorController',
    ['$scope', '$location', '$window', '$routeParams', 'AssessorService', 'EnumsService',
    function ($scope, $location, $window, $routeParams, AssessorService, EnumsService) {
        $scope.assessorModel = {
            id: null,
            userName: '',
            password: '',
            confirmPassword: '',
            email: '',
            title: '',
            firstName: '',
            surname: '',
            idOrPassport: '',
            genderType: '',
            telephone: '',
            addressLine1: '',
            addressLine2: '',
            city: '',
            postalCode: ''


        };


        $scope.genderTypes = EnumsService.genderTypes();

        $scope._id = $scope.assessorModel.id
        $scope._id = $routeParams.id;

        if ($scope._id > 0) {
            AssessorService.AssessorGet($scope._id).then(
            function (result) {
                $scope.assessorModel.id = result.data.id;
                $scope.assessorModel.userId = result.data.userId;
                $scope.assessorModel.userName = result.data.userName;
                $scope.assessorModel.password = result.data.password;
                $scope.assessorModel.confirmPassword = result.data.password;
                $scope.assessorModel.email = result.data.email;
                $scope.assessorModel.title = result.data.title;
                $scope.assessorModel.firstName = result.data.firstName;
                $scope.assessorModel.surname = result.data.surname;
                $scope.assessorModel.idOrPassport = result.data.idOrPassport;
                $scope.assessorModel.genderType = result.data.genderType;
                $scope.assessorModel.telephone = result.data.telephone;
                $scope.assessorModel.addressLine1 = result.data.addressLine1;
                $scope.assessorModel.addressLine2 = result.data.addressLine2;
                $scope.assessorModel.city = result.data.city;
                $scope.assessorModel.postalCode = result.data.postalCode

            },
             function (error) {
                 handleError(error);
             });
        };

        $scope.submitForm = function () {

            $scope.$broadcast('show-errors-check-validity');

            if ($scope.assessorForm.$invalid)
                return;


            var result = AssessorService.SaveAssessor($scope.assessorModel.id,
                                       $scope.assessorModel.userId,
                                       $scope.assessorModel.userName,
                                       $scope.assessorModel.password,
                                       $scope.assessorModel.confirmPassword,
                                       $scope.assessorModel.email,
                                       $scope.assessorModel.title,
                                       $scope.assessorModel.firstName,
                                       $scope.assessorModel.surname,
                                       $scope.assessorModel.idOrPassport,
                                       $scope.assessorModel.genderType,
                                       $scope.assessorModel.telephone,
                                       $scope.assessorModel.addressLine1,
                                       $scope.assessorModel.addressLine2,
                                       $scope.assessorModel.city,
                                       $scope.assessorModel.postalCode
                                       );



            result.then(function (result) {

                $location.path('/SaveAssessorSchool/' + $scope.assessorModel.userName);
            },
      function (error) {
          alert('Some error occured');
      });

        }

        $scope.resetForm = function () {
            $scope.$broadcast('show-errors-reset');
        }

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        }
        $scope.newAssessor = function () {
            $location.path("/AssessorEdit/0");
        };
        $scope.NewAssessorSchoolForm = function () {
            $location.path('/SaveAssessorSchool/' + $scope.assessorModel.id);
        };
        $scope.NewAssessorSchoolForm12 = function () {
            $location.path('/SaveAssessorSchool2/' + $scope.assessorModel.id);
        };
        $scope.sortKeyOrder = {
            key: 'assessorName',
            order: 'ASC',
        };



        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.maxSize = 5;
        $scope.recordsPerPage = 20;
        $scope.numberOfPageButtons = 5;

        $scope.sort = function (col) {
            if ($scope.sortKeyOrder.key === col) {
                if ($scope.sortKeyOrder.order == 'ASC')
                    $scope.sortKeyOrder.order = 'DESC';
                else
                    $scope.sortKeyOrder.order = 'ASC';
            } else {
                $scope.sortKeyOrder.key = col;
                $scope.sortKeyOrder.order = 'ASC';
            }
            loadGrid();
        };


        var loadGrid = function () {
            var searchFor = '';

            AssessorService.AssessorGrid($scope.currentPage, $scope.recordsPerPage,
                                      $scope.sortKeyOrder.key, $scope.sortKeyOrder.order, searchFor).then(
                function (result) {
                    $scope.assessorGrid = result.data.results;
                    $scope.totalItems = result.data.recordCount;
                },
                function (error) {
                    alert("an error occured: unable to get data");
                });


        };


        $scope.pageChanged = function () {
            loadGrid();
        };


        loadGrid();
    }

    ]);