﻿AngularApp.controller('LearnerAsssessorController',
    ['$scope', '$location', '$window', '$routeParams', 'LearnerAssessorService', 'AssessorService',
    function ($scope, $location, $window, $routeParams, LearnerAssessorService, AssessorService) {

        $scope.learnerAssessorModel = {
            id: null,
            assessorId: '',
            learnerId: ''

        };

        $scope.learnerAssessorModel.id = $routeParams.id;

        if ($scope.learnerAssessorModel.id > 0) {
            LearnerAssessorService.GetLearnerAssessor($scope.learnerAssessorModel.id).then(
                function (result) {

                    $scope.learnerAssessorModel.id = result.data.id;
                    $scope.learnerAssessorModel.assessorId = result.data.assessorId;
                    $scope.learnerAssessorModel.learnerId = result.data.learnerId;
                   
                },


                function (error) {
                    handleError(error);
                });
        };


        $scope.resetForm = function () {
            $scope.$broadcast('show-errors-reset');
        }

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        }
    }
    ]);