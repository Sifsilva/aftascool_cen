﻿AngularApp.controller('AssessorSchoolController',
    ['$scope', '$location', '$window', '$routeParams', 'AssessorSchoolService', 'SchoolService', 'AssessorService',
    function ($scope, $location, $window, $routeParams, AssessorSchoolService, SchoolService, AssessorService) {

        $scope.assessorSchoolModel = {
            id: null,
            assessorId: '',
            schoolId: '',
            startDate: '',
            endDate: ''

        };

        var info = $routeParams.id;
        var testing = true;
        AssessorService.AssessorList().then(
        function (result) {
            $scope.assessorList = result.data;
            angular.forEach($scope.assessorList, function (test) {
                if (testing == true) {
                    if (info == test.description) {

                        $scope.assessorSchoolModel.assessorId = test.id;
                        return $scope.assessorSchoolModel.assessorId;
                    }
                    if ($scope.assessorSchoolModel.assessorId > 0)
                    { testing = false; }
                }
            });
        },
         function (error) {
             $scope.hasError = true;
             $scope.errorMessage = error.statusText;
         });



       

        /*  $scope.assessorSchoolModel.id=$routeParams.id;
  
          if($scope.assessorSchoolModel.id > 0){
              AssessorSchoolService.GetAssessorSchool($scope.assessorSchoolModel.id).then(
                  function (result){
                      $scope.assessorSchoolModel.id=result.data.id;
                      $scope.assessorSchoolModel.assessorId=result.assessorId;
                      $scope.assessorSchoolModel.schoolId=result.schoolId;
                      $scope.assessorSchoolModel.startDate=result.startDate;
                      $scope.assessorSchoolModel.endDate=result.endDate;
                   
                  },
                  function (error){
                      handleError(error);
                  });
  
          };*/
        SchoolService.SchoolList().then(
        function (result) {
            $scope.schoolList = result.data;
        },
        function (error) {
            $scope.hasError = true;
            $scope.errorMessage = error.statusText;
        });
        $scope.submitForm = function () {

            $scope.$broadcast('show-errors-check-validity');

            if ($scope.NewAssessorSchoolForm.$invalid)
                return;

            var result = AssessorSchoolService.saveAssessorSchool($scope.assessorSchoolModel.id,
                                                 $scope.assessorSchoolModel.assessorId,
                                                 $scope.assessorSchoolModel.schoolId,
                                                 $scope.assessorSchoolModel.startDate,
                                                 $scope.assessorSchoolModel.endDate



                );
            result.then(function (result) {

                $location.path('/home');
            },

             function (error) {
                 handleError(error);
             });
        }

        $scope.resetForm = function () {
            $scope.$broadcast('show-errors-reset');
        }

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        }

        $scope.newAssessorSchool = function () {
            $location.path("/SaveAssessorSchool");
        };

        var handleError = function (error) {
            $scope.hasError = true;
            $scope.errorMessage = error.statusText;
        }
    }
    ]);