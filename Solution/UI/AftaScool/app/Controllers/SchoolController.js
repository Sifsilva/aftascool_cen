﻿AngularApp.controller('SchoolController',
    ['$scope', '$location', '$window', '$routeParams', 'EnumsService','SchoolService',
        function ($scope, $location, $window, $routeParams, EnumsService, SchoolService) {
            $scope.schoolModel = {
                id: null,
                schoolName: '',
                schoolType:'',
                addressLine1: '',
                addressLine2: '',
                city: '',
                postalCode: '',
                telephone: '',
                location: null 
            };


            $scope.schoolModel.id = $routeParams.id;
            if ($scope.schoolModel.id > 0) {
                SchoolService.SchoolGet($scope.schoolModel.id).then(
                    function (result) {
                        $scope.schoolModel.id = result.data.id;
                        $scope.schoolModel.schoolType = result.data.schoolType;
                        $scope.schoolModel.schoolName = result.data.schoolName;
                        $scope.schoolModel.addressLine1 = result.data.addressLine1;
                        $scope.schoolModel.addressLine2 = result.data.addressLine2;
                        $scope.schoolModel.city = result.data.city;
                        $scope.schoolModel.postalCode = result.data.postalCode;
                        $scope.schoolModel.telephone = result.data.telephone;
                        $scope.schoolModel.location = result.data.location;
                    },
                    function (error) {
                        handleError(error);
                    });
            };
            $scope.schoolTypes = EnumsService.schoolTypes();

            $scope.submitForm = function () {

                $scope.$broadcast('show-errors-check-validity');

                if ($scope.NewSchoolForm.$invalid)
                    return;

                var result = SchoolService.SchoolSave($scope.schoolModel.id,
                                                      $scope.schoolModel.schoolName,
                                                      $scope.schoolModel.schoolType,
                                                      $scope.schoolModel.addressLine1,
                                                      $scope.schoolModel.addressLine2,
                                                      $scope.schoolModel.city,
                                                      $scope.schoolModel.postalCode,
                                                      $scope.schoolModel.telephone,
                                                      $scope.schoolModel.location

                     );
              
                result.then(function (result) {
                    $window.history.back();

                },
                 function (error) {
                     handleError(error);
                 });
            }
            $scope.resetForm = function () {
                $scope.$broadcast('show-errors-reset');
            }

            $scope.cancelForm = function () {
                $scope.$broadcast('show-errors-reset');
                $window.history.back();
            }
            $scope.newSchool = function () {
                $location.path("/SchoolEdit/0");
            };

            var handleError = function (error) {
                $scope.hasError = true;
                $scope.errorMessage = error.statusText;
            }
            var ErrorSuccess = function () {
                $scope.handleSuccess = true;
                $scope.errorMessage = error.statusText;
            }
            $scope.sortKeyOrder = {
                key: 'schoolName',
                order: 'ASC',
            };

  
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.maxSize = 5; 
            $scope.recordsPerPage = 20;
            $scope.numberOfPageButtons = 5;

            $scope.sort = function (col) {
                if ($scope.sortKeyOrder.key === col) {
                    if ($scope.sortKeyOrder.order == 'ASC')
                        $scope.sortKeyOrder.order = 'DESC';
                    else
                        $scope.sortKeyOrder.order = 'ASC';
                } else {
                    $scope.sortKeyOrder.key = col;
                    $scope.sortKeyOrder.order = 'ASC';
                }
                loadGrid();
            };


            var loadGrid = function () {
                var searchFor = '';

                SchoolService.SchoolGrid($scope.currentPage, $scope.recordsPerPage,
                                          $scope.sortKeyOrder.key, $scope.sortKeyOrder.order, searchFor).then(
                    function (result) {
                        $scope.schoolGrid = result.data.results;
                        $scope.totalItems = result.data.recordCount;
                    },
                    function (error) {
                        alert("an error occured: unable to get data");
                    });


            };


            $scope.pageChanged = function () {
                loadGrid();
            };


            loadGrid();

        }
    ]);