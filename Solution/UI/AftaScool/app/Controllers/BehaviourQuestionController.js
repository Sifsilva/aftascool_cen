﻿AngularApp.controller('BehaviourQuestionController',
    ['$scope', '$location', '$window', '$routeParams', 'BehaviourQuestionService', 'BehaviourService',
    function ($scope, $location, $window, $routeParams, BehaviourQuestionService, BehaviourService) {

        $scope.behaviourQuestionModel = {
            id: null,
            behaviourId: '',
            minimumWeight: '',
            maximumWeight:''

        };
        $scope.test = $routeParams.id;
       
        BehaviourService.BehaviourList().then(
          function (result) {
          $scope.behaviourList = result.data;
         },
         function (error) {
          $scope.hasError = true;
          $scope.errorMessage = error.statusText;
         });

        $scope.submitForm = function () {

            $scope.$broadcast('show-errors-check-validity');

            if ($scope.NewBehaviourQuestionForm.$invalid)
                return;


            var result = BehaviourQuestionService.BehaviourQuestionSave($scope.behaviourQuestionModel.id,
                                                                        $scope.behaviourQuestionModel.behaviourId,
                                                                        $scope.behaviourQuestionModel.minimumWeight,
                                                                        $scope.behaviourQuestionModel.maximumWeight



                );


            result.then(function (result) {
                $location.path('/SaveQuestionnaireQuestion/' + $scope.test);
            },
            function (error) {
                alert('Some error occured');
            });
        }

        $scope.resetForm = function () {
            $scope.$broadcast('show-errors-reset');
        }

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        }
    }
    ]);