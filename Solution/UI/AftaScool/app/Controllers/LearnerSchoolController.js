﻿AngularApp.controller('LearnerSchoolController',
    ['$scope', '$location', '$window', '$routeParams', 'LearnerSchoolService', 'SchoolService', 'LearnerService',
    function ($scope, $location, $window, $routeParams, LearnerSchoolService, SchoolService, LearnerService) {

      //  $scope.ModifiedDate = $filter("date")(Date.now(), 'yyyy-MM-dd');
        $scope.learnerSchoolModel = {
            id: null,
            learnerId:'',
            schoolId: '',
            startDate: '',
            endDate:''

        };

       
        var info = $routeParams.id;
        var testing = true;
        LearnerService.LearnerList().then(
        function (result) {
            $scope.learnerList = result.data;

            angular.forEach($scope.learnerList, function (test) {
                if (testing==true) {
                    if (info == test.description) {

                        $scope.learnerSchoolModel.learnerId = test.id;
                        return $scope.learnerSchoolModel.learnerId;
                    }
                    if ($scope.learnerSchoolModel.learnerId > 0)
                    { testing = false; }
                          
                }

            });
        },
         function (error) {
             $scope.hasError = true;
             $scope.errorMessage = error.statusText;
         });



     
        /*
        if ($scope.learnerSchoolModel.id > 0) {
            LearnerSchoolService.GetLearnerSchools($scope.learnerSchoolModel.id).then(
                function (result) {
                    $scope.learnerSchoolModel.id = result.data.id;
                    $scope.learnerSchoolModel.learnerId = result.data.learnerId;
                    $scope.learnerSchoolModel.schoolId = result.schoolId;
                    $scope.learnerSchoolModel.startDate = result.startDate;
                    $scope.learnerSchoolModel.endDate = result.endDate;

                },
                function (error) {
                    handleError(error);
                });

        };*/

        SchoolService.SchoolList().then(
         function (result) {
             $scope.schoolList = result.data;
         },
          function (error) {
              $scope.hasError = true;
              $scope.errorMessage = error.statusText;
          });
        $scope.submitForm = function () {

            $scope.$broadcast('show-errors-check-validity');

            if ($scope.NewLearnerSchoolForm.$invalid)
                return;

            var result = LearnerSchoolService.SaveLearnerSchool($scope.learnerSchoolModel.id,
                                                                $scope.learnerSchoolModel.learnerId,
                                                                $scope.learnerSchoolModel.schoolId,
                                                                $scope.learnerSchoolModel.startDate,
                                                                $scope.learnerSchoolModel.endDate



                );

            result.then(function (result) {
                $location.path('/home');
            },
           function (error) {
               alert('Some error occured');
           });
        }

        $scope.resetForm = function () {
            $scope.$broadcast('show-errors-reset');
        }

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        }
    }
    ]);