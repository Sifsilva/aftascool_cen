﻿var AngularApp = angular.module('AngularApp', ['ngRoute', 'ui.bootstrap', 'validation.match', 'ui.bootstrap.showErrors', 'chieffancypants.loadingBar','ngAnimate']);

AngularApp.config(
    ['$routeProvider', '$httpProvider', '$locationProvider',
    function ($routeProvider, $httpProvider, $locationProvider) {
        $locationProvider.hashPrefix('!');
        $routeProvider
        .when("/home", {
            templateUrl: "app/Views/Home/Home.html",
            controller: "HomeController"
        })
        .when('/login', {
            templateUrl: 'app/Views/Account/Login.html',
            controller: 'LoginController'
        })
        .when('/register', {
            templateUrl: 'app/Views/Account/Register.html',
            controller: 'RegisterController'
        })
        .when('/LearnerEdit/:id', {
            templateUrl: 'app/Views/Learner/SaveLearner.html',
            controller: 'LearnerController'
        })
        .when('/LearnerMaintenance', {
            templateUrl: 'app/Views/Learner/LearnerMaintenance.html',
            controller: 'LearnerController'
        })
        .when('/SaveQuestionnaire', {
            templateUrl: 'app/Views/Questionnaire/SaveQuestionnaire.html',
            controller: 'QuestionnaireController'
        })
        .when('/SaveQuestionnaireQuestion', {
            templateUrl: 'app/Views/Questionnaire/SaveQuestionnaireQuestion.html',
            controller: 'QuestionnaireQuestionController'
        })
        .when('/SchoolEdit/:id', {
            templateUrl: 'app/Views/School/SaveSchool.html',
            controller: 'SchoolController'
        })
        .when('/SchoolMaintenance', {
            templateUrl: 'app/Views/School/SchoolMaintenance.html',
            controller: 'SchoolController'
        })
        .when('/Behaviour', {
            templateUrl: 'app/Views/Behaviour/SaveBehaviour.html',
            controller: 'BehaviourController'
        })
        .when('/BehaviourQuestionSave', {
            templateUrl: 'app/Views/BehaviourQuestion/BehaviourQuestionSave.html',
            controller: 'BehaviourQuestionController'
        })
        .when('/AssessorEdit/:id', {
            templateUrl: 'app/Views/Assessor/SaveAssessor.html',
            controller: 'AssessorController'
        })
        .when('/AssessorMaintenance', {
            templateUrl: 'app/Views/Assessor/AssessorMaintenance.html',
            controller: 'AssessorController'
        })
        .when('/SaveLearnerSchool', {
            templateUrl: 'app/Views/LearnerSchool/SaveLearnerSchool.html',
            controller: 'LearnerSchoolController'
        })
        .when('/SaveAssessorSchool/:id', {
            templateUrl: 'app/Views/AssessorSchool/SaveAssessorSchool.html',
            controller: 'AssessorSchoolController'
        })
        .when('/SaveAssessorSchool2/:id', {
            templateUrl: 'app/Views/AssessorSchool/SaveAssessorSchool2.html',
            controller: 'AssessorSchoolController'
        })

    .otherwise({
        redirectTo: '/home'
    });

        $locationProvider.html5Mode(true);
        $httpProvider.interceptors.push('AuthHttpResponseInterceptor');

        //initialize get if not there
        if (!$httpProvider.defaults.headers.get) {
            $httpProvider.defaults.headers.get = {};
        }

        $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
        $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
        $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';

    }]);

