﻿AngularApp.factory('AssessorService',
    ['$http',
        function ($http) {
            var _saveAssessor = function (id,userId,userName, password,confirmPassword, email, title,
                            firstName, surname, idOrPassport, genderType,
                            telephone, addressLine1, addressLine2, city, postalCode)
            {
             return $http.post("/Assessor/SaveAssessor", {
                 Id: id,
                 UserId: userId,
                    UserName: userName,
                    Password: password,
                    ConfirmPassword: confirmPassword,
                    Email: email,
                    Title: title,
                    FirstName: firstName,
                    SurName: surname,
                    idOrPassport: idOrPassport,
                    GenderType: genderType,
                    Telephone: telephone,
                    AddressLine1: addressLine1,
                    AddressLine2: addressLine2,
                    City: city,
                    PostalCode: postalCode



                });
            };

           var _assessorList = function () {
                return $http.get("/Assessor/AssessorsList");
            }
            

            var _assessorGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
                var req = {
                    CurrentPage: currentPage,
                    RecordsPerPage: recordsPerPage,
                    SortKey: sortKey,
                    SortOrder: sortOrder,
                    Searchfor: searchfor
                }
                return $http.put("/Assessor/AssessorGrid", req);
            }
            var _assessorGet = function (assessorId) {
                return $http.get("/Assessor/AssessorGet/" + assessorId);
            }

            return{
                SaveAssessor: _saveAssessor,
                AssessorGrid: _assessorGrid,
                AssessorList: _assessorList,
                AssessorGet: _assessorGet
            }

         }
    ]);