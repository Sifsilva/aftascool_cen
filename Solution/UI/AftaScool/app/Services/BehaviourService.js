﻿AngularApp.factory('BehaviourService',
    ['$http',
    function ($http) {

        var saveBahviour = function (id, type) {
            return $http.post("/Behaviour/SaveBehaviour", {
                Id: id,
                Type: type
            });
        };
        var _behaviourList = function () {
            return $http.get("/Behaviour/BehaviourList");
        };
        return {
            SaveBehaviour: saveBahviour,
            BehaviourList: _behaviourList
        }
    }
    ]);