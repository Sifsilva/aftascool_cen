﻿AngularApp.factory('QuestionnaireService',
     ['$http',
         function ($http) {
             var _questionnaireSave = function (id, assessorId, learnerId, questionnaireDate) {
                 return $http.post("/Questionnaire/QuestionnaireSave", {
                     Id: id,
                     AssessorId: assessorId,
                     LearnerId: learnerId,
                     QuestionnaireDate: questionnaireDate

                 });

             };
             return {
                 QuestionnaireSave: _questionnaireSave
               
             }
         }
     ]);