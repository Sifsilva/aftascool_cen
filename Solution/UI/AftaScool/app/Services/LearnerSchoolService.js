﻿AngularApp.factory('LearnerSchoolService',
    ['$http',
    function ($http) {

        var saveLearnerSchool = function (id, learnerId, schoolId, startDate, endDate) {
            return $http.post("/LearnerSchool/saveLearnerSchool", {
                Id: id,
                LearnerId: learnerId,
                SchoolId: schoolId,
                StartDate: startDate,
                EndDate: endDate
                
     
            });
        };

        return {
            SaveLearnerSchool: saveLearnerSchool
        }
    }
]);