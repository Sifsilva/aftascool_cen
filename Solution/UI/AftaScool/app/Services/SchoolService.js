﻿AngularApp.factory('SchoolService',
     ['$http',
         function ($http) {
             var _schoolSave = function (id, schoolName,schoolType, addressLine1, addressLine2, city, postalCode, telephone, location) {
                 return $http.post("/School/SchoolSave", {
                     Id: id,
                     SchoolName: schoolName,
                     SchoolType:schoolType,
                     AddressLine1: addressLine1,
                     AddressLine2: addressLine2,
                     City: city,
                     PostalCode: postalCode,
                     Telephone: telephone,
                     Location: location

                 });
             };
             var _schoolList = function () {
                 return $http.get("/School/SchoolList");
             };
             var _schoolGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
                 var req = {
                     CurrentPage: currentPage,
                     RecordsPerPage: recordsPerPage,
                     SortKey: sortKey,
                     SortOrder: sortOrder,
                     Searchfor: searchfor
                 }
                 return $http.put("/School/SchoolGrid", req);
             }
             var _schoolGet = function (schoolId) {
                 return $http.get("/School/SchoolGet/" + schoolId);
             }
             return {
                 SchoolSave: _schoolSave,
                 SchoolList: _schoolList,
                 SchoolGrid: _schoolGrid,
                 SchoolGet: _schoolGet
             }
         }
     ]);