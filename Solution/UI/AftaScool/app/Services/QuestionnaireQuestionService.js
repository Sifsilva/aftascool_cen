﻿AngularApp.factory('QuestionnaireQuestionService',
     ['$http',
         function ($http) {
             var _questionnaireQuestionSave = function (id, questionnaireId, behaviourQuestionId, trait) {
                 return $http.post("/QuestionnaireQuestion/QuestionnaireQuestionSave", {
                     Id: id,
                     QuestionnaireId: questionnaireId,
                     BehaviourQuestionId: behaviourQuestionId,
                     Trait: trait

                 });
             };
             var _questionnaireQuestionList = function () {
                 return $http.get("/QuestionnaireQuestion/QuestionnaireQuestionList");
             };
             var _questionnaireQuestionGet = function (questionnaireQuestionId) {
                 return $http.get("/QuestionnaireQuestion/QuestionnaireQuestionGet/" + questionnaireQuestionId);
             }
             return {
                 QuestionnaireQuestionSave: _questionnaireQuestionSave,
                 QuestionnaireQuestionList: _questionnaireQuestionList,
                 QuestionnaireQuestionGet: _questionnaireQuestionGet
             }
         }
     ]);
