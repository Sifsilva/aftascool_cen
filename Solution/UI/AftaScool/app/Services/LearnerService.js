﻿AngularApp.factory('LearnerService',
    ['$http',
   
     function ($http){ 
         var _learnerSave = function (id, learnerName, learnerSurname, grade, idorpassport, genderType, addressline1, addressline2, city, postalCode, telephone) {
                return $http.post("/Learner/LearnerSave", {


                    Id:id,
                    LearnerName: learnerName,
                    LearnerSurname: learnerSurname,
                    Grade: grade,
                    IdPassportNum: idorpassport,
                    Gender: genderType,
                    AddressLine1: addressline1,
                    AddressLine2: addressline2,
                    City: city,
                    PostalCode: postalCode,
                    Telephone:telephone
              


                });
         };

         var _learnerList = function () {
             return $http.get("/Learner/LearnerList");
         };

         var _learnerGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
             var req = {
                 CurrentPage: currentPage,
                 RecordsPerPage: recordsPerPage,
                 SortKey: sortKey,
                 SortOrder: sortOrder,
                 Searchfor: searchfor
             }
             return $http.put("/Learner/LearnerGrid", req);
         }
         var _learnerGet = function (learnerId) {
             return $http.get("/Learner/LearnerGet/" + learnerId);
         }



            return {
                LearnerSave: _learnerSave,
                LearnerList: _learnerList,
                LearnerGrid: _learnerGrid,
                LearnerGet: _learnerGet
            }


        }

]);