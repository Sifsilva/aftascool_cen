﻿AngularApp.factory('AssessorSchoolService',
    ['$http',
    function ($http) {

        var _SaveAssessorSchool = function (id, assessorId, schoolId, startDate, endDate) {
            return $http.post("/AssessorSchool/saveAssessorSchool", {
                Id: id,
                AssessorId: assessorId,
                SchoolId: schoolId,
                StartDate: startDate,
                EndDate: endDate
            });
        };

        var _assessorSchoolList = function () {
            return $http.get("/AssessorSchool/AssessorSchoolList");
        };

        
        var _getAssessorSchool = function (assessorSchoolId) {
            return $http.get("/AssessorSchool/GetAssessorSchool/" + assessorSchoolId);
        }

        return {
            saveAssessorSchool: _SaveAssessorSchool,
            AssessorSchoolList: _assessorSchoolList,
            GetAssessorSchool: _getAssessorSchool
        }
    }

    ]);