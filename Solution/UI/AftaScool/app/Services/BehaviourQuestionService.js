﻿AngularApp.factory('BehaviourQuestionService',
    ['$http',
    function ($http) {

        var behaviourQuestionSave = function (id, behaviourId, minimumWeight, maximumWeight) {
            return $http.post("/BehaviourQuestion/BehaviourQuestionSave", {
                Id: id,
                BehaviourId: behaviourId,
                MinimumWeight: minimumWeight,
                MaximumWeight: maximumWeight
            });
        };
        var behaviourQuestionList = function () {
            return $http.get("/BehaviourQuestion/BehaviourQuestionList")
        }

        return {
            BehaviourQuestionSave: behaviourQuestionSave
        }
    }
    ]);