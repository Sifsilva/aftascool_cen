﻿AngularApp.factory('LearnerAssessorService',
     ['$http',
         function ($http) {
             var _learnerAssessorSave = function (id, assessorId, learnerId) {
                 return $http.post("/LearnerAssessor/SaveLearnerAssessor", {
                     Id: id,
                     AssessorId: assessorId,
                     LearnerId: learnerId

                 });

             };
             return {
                 SaveLearnerAssessor: _learnerAssessorSave

             }
         }
     ]);