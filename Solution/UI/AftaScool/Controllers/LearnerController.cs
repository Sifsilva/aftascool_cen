﻿using AftaScool.BL.Context;
using AftaScool.BL.Provider.LearnerData;
using AftaScool.Models;
using AftaScool.Models.Learner;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AftaScool.BL.Entities.LearnerData;

namespace AftaScool.Controllers
{

    [Authorize]
    public class LearnerController : TCRControllerBase
    {

        #region Ctor

        public ILearnerProvider LearnerProvider { get; set; }
        public DataContext DataContext { get; set; }
        private bool _MustDisposeContext = true;

        public LearnerController()
        {
            DataContext = new DataContext();
            LearnerProvider  = new LearnerProvider (DataContext, CurrentUser);
        }

        public LearnerController(ILearnerProvider learnerProvider)
        {
            _MustDisposeContext = false;
            LearnerProvider = learnerProvider;
        }

        protected override void Dispose(bool disposing)
        {
            if (_MustDisposeContext)
                DataContext.Dispose();
            base.Dispose(disposing);
        }

        #endregion
        // Save: Learner

      

        public ActionResult LearnerSave(LearnerModel model)
        {


            try
            {
                //returns a condition based on the request.
                if (!ModelState.IsValid)
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error details");

                var learner = LearnerProvider.LearnerSave(model.Id,model.LearnerName,model.LearnerSurname,model.Grade,
                    model.IdPassportNum,model.Gender,model.AddressLine1,model.AddressLine2,model.City,model.PostalCode,model.Telephone);
                model.Id = learner.Id;
              

               
               
                return SerializeToAngular(model);
            }
            catch (LearnerException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }

            

        }
        
        
        public ActionResult LearnerList()
        {
            
            var itms = LearnerProvider.GetLearners()
                                     .Where(a => a.Status == BL.Types.StatusType.Active)
                                     .Select(b => new KeyValueModel { Id = b.Id, Description = b.IdPassportNum})
                                     .OrderBy(b => b.Description)
                                     .ToList();
            return SerializeToAngular(itms);
        }

        [HttpPut]
        public ActionResult LearnerGrid(GridModel model)
        {

            int begin = SetupGridParams(model);

            var filteredQuery = LearnerProvider.GetLearners()
                                                        .Select(a => new LearnerGridModel()
                                                        {
                                                            Id = a.Id,
                                                            LearnerName = a.LearnerName,
                                                            LearnerSurname = a.LearnerSurname,
                                                            IdPassportNum = a.IdPassportNum,
                                                            Grade = a.Grade,
                                                            GenderType = a.Gender,
                                                            StatusType = a.Status,
                                                            AddressLine1 = a.AddressLine1,
                                                            AddressLine2 = a.AddressLine2,
                                                            City = a.City,
                                                            PostalCode = a.PostalCode,
                                                            Telephone = a.Telephone
                                                        });
            if (model.Searchfor != "null")
            {
                filteredQuery = filteredQuery.Where(r => r.LearnerName.Contains(model.Searchfor)
                                                    || r.LearnerSurname.Contains(model.Searchfor)
                                                    || r.IdPassportNum.Contains(model.Searchfor)
                                                    || r.Grade.Contains(model.Searchfor)
                                                    || r.AddressLine1.Contains(model.Searchfor)
                                                    || r.AddressLine2.Contains(model.Searchfor)                                                   
                                                    || r.City.Contains(model.Searchfor)
                                                    || r.PostalCode.Contains(model.Searchfor)
                                                    || r.Telephone.Contains(model.Searchfor)
                                                    );
            }
            //Get Reord count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.LearnerName); //default sort order

            //Setup sort order
            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "learnerName":
                            filteredQuery = filteredQuery.OrderBy(r => r.LearnerName);
                            break;
                       case "learnerSurname":
                            filteredQuery = filteredQuery.OrderBy(r => r.LearnerSurname);
                            break;
                        case "status":
                            filteredQuery = filteredQuery.OrderBy(r => r.StatusType);
                            break;
                        case "idorpassport":
                            filteredQuery = filteredQuery.OrderBy(r => r.IdPassportNum);
                            break;
                        case "city":
                            filteredQuery = filteredQuery.OrderBy(r => r.City);
                            break;
                        case "addressline1":
                            filteredQuery = filteredQuery.OrderBy(r => r.AddressLine1);
                            break;
                        case "addressline2":
                            filteredQuery = filteredQuery.OrderBy(r => r.AddressLine2);
                            break;
                        case "postalCode":
                            filteredQuery = filteredQuery.OrderBy(r => r.PostalCode);
                            break;
                        case "telephone":
                            filteredQuery = filteredQuery.OrderBy(r => r.Telephone);
                            break;
                        case "gender":
                            filteredQuery = filteredQuery.OrderBy(r => r.Gender);
                            break;
                        case "grade":
                            filteredQuery = filteredQuery.OrderBy(r => r.Grade);
                            break;

                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "learnerName":
                            filteredQuery = filteredQuery.OrderBy(r => r.LearnerName);
                            break;
                        case "learnerSurname":
                            filteredQuery = filteredQuery.OrderBy(r => r.LearnerSurname);
                            break;
                        case "status":
                            filteredQuery = filteredQuery.OrderBy(r => r.StatusType);
                            break;
                        case "idorpassport":
                            filteredQuery = filteredQuery.OrderBy(r => r.IdPassportNum);
                            break;
                        case "city":
                            filteredQuery = filteredQuery.OrderBy(r => r.City);
                            break;
                        case "addressline1":
                            filteredQuery = filteredQuery.OrderBy(r => r.AddressLine1);
                            break;
                        case "addressline2":
                            filteredQuery = filteredQuery.OrderBy(r => r.AddressLine2);
                            break;
                        case "postalCode":
                            filteredQuery = filteredQuery.OrderBy(r => r.PostalCode);
                            break;
                        case "telephone":
                            filteredQuery = filteredQuery.OrderBy(r => r.Telephone);
                            break;
                        case "gender":
                            filteredQuery = filteredQuery.OrderBy(r => r.Gender);
                            break;
                        case "grade":
                            filteredQuery = filteredQuery.OrderBy(r => r.Grade);
                            break;
                    }
                    break;
            }

            //setup paging
            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);


            return SerializeToAngular(new GridResultModel<LearnerGridModel>(filteredQuery.ToList(), totalNumberOfRecords));
        }


        public ActionResult LearnerGet(long? id)
        {
            if (id == null)
                id = 0;

            try
            {
            
                var model = LearnerProvider.GetLearners().Where(a => a.Id == id).Select(a => new LearnerModel()
                {
                    Id = a.Id,
                    LearnerName = a.LearnerName,
                    LearnerSurname = a.LearnerSurname,
                    IdPassportNum = a.IdPassportNum,
                    Grade = a.Grade,
                    AddressLine1 = a.AddressLine1,
                    AddressLine2 = a.AddressLine2,
                    Gender = a.Gender,
                    City = a.City,
                    PostalCode = a.PostalCode,
                    Telephone = a.Telephone
                }).Single();
                return SerializeToAngular(model);
            }
            catch (LearnerException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }
        //public ActionResult Delete(int id)
        //{
        //    try
        //    {
        //        Learner learn = DataContext.LearnerSet.Find();
        //        DataContext.LearnerSet.Remove(learn);
        //        DataContext.SaveChanges();
        //    }
        //    catch (Exception)
        //    {
        //        return RedirectToAction("Delete", new { id = id, saveChangesError = true });
        //    }
        //    return RedirectToAction("");
        //}
    }
}