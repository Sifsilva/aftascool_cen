﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AftaScool.BL.Provider.AssessorData;
using AftaScool.BL.Context;
using System.Net;
using System.Web.Mvc;
using AftaScool.Models.Assessor;
using AftaScool.Models;

namespace AftaScool.Controllers
{

    [Authorize]

    #region Acor
    public class AssessorController : TCRControllerBase
    {
        public IAssessorProvider assessorProvider { get; set; }
        public DataContext DataContext { get; set; }
        private bool _MustDisposeContext = true;


        public AssessorController()
        {
            DataContext = new DataContext();
            assessorProvider = new AssessorProvider(DataContext, CurrentUser);
        }
        public AssessorController(IAssessorProvider AssessorProvider)
        {
            _MustDisposeContext = false;
            assessorProvider = AssessorProvider;
        }

        protected override void Dispose(bool disposing)
        {
            if (_MustDisposeContext)
                DataContext.Dispose();
            base.Dispose(disposing);
        }

    #endregion

        public ActionResult SaveAssessor(AssessorModel model)
        {

            try
            {
                if (!ModelState.IsValid)
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error ");


                var ass = assessorProvider.SaveAssessor(model.Id, model.UserName, model.Password, model.Email, model.Title, model.FirstName,
                                       model.Surname, model.IDOrPassport, model.GenderType, model.Telephone,
                                model.AddressLine1, model.AddressLine2, model.City, model.PostalCode);

                model.Id = ass.Id;
                return SerializeToAngular(model);
            }
            catch (AssessorException err)
            {

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, err.Message);
            }

        }
        public ActionResult AssessorsList()
        {
            var assessors = assessorProvider.GetAssessor().Where(a => a.Id == a.Id)
                            .Select(b => new KeyValueModel { Id = b.Id, Description = b.UserIdentities.UserName })
                            .OrderBy(b => b.Description).ToList();


            return SerializeToAngular(assessors);
        }
        [HttpPut]
        public ActionResult AssessorGrid(GridModel model)
        {
            var begin = SetupGridParams(model);

            var filteredQuery = assessorProvider.GetAssessor()
                                    .Select(a => new AssessorGridModel()
                                    {
                                        Id = a.Id,
                                        UserName = a.UserIdentities.UserName,
                                        Email = a.UserIdentities.EmailAddress,
                                        Title = a.UserIdentities.Title,
                                        FirstName = a.UserIdentities.FirstName,
                                        Surname = a.UserIdentities.Surname,
                                        idOrPassport = a.UserIdentities.IdPassportNum,
                                        GenderType = a.UserIdentities.Gender,
                                        Telephone = a.UserIdentities.Telephone,
                                        AddressLine1 = a.UserIdentities.AddressLine1,
                                        AddressLine2 = a.UserIdentities.AddressLine2,
                                        City = a.UserIdentities.City,
                                        PostalCode = a.UserIdentities.PostalCode,


                                    });

            if (model.Searchfor != "null")
            {
                filteredQuery = filteredQuery.Where(r => r.UserName.Contains(model.Searchfor)
                                                    || r.Title.Contains(model.Searchfor)
                                                    || r.FirstName.Contains(model.Searchfor)
                                                    || r.Surname.Contains(model.Searchfor)
                                                    || r.idOrPassport.Contains(model.Searchfor)
                                                    || r.Telephone.Contains(model.Searchfor)
                                                    || r.Email.Contains(model.Searchfor)
                                                    || r.AddressLine1.Contains(model.Searchfor)
                                                    || r.AddressLine2.Contains(model.Searchfor)
                                                    || r.City.Contains(model.Searchfor)
                                                    || r.PostalCode.Contains(model.Searchfor)

                                                    );
            }

            var totalrecords = filteredQuery.Count();
            //default sort
            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.UserName);
            //Setup sort order
            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "username":
                            filteredQuery = filteredQuery.OrderBy(s => s.UserName);
                            break;
                        case "title":
                            filteredQuery = filteredQuery.OrderBy(s => s.Title);
                            break;
                        case "firstname":
                            filteredQuery = filteredQuery.OrderBy(s => s.FirstName);
                            break;
                        case "surname":
                            filteredQuery = filteredQuery.OrderBy(s => s.Surname);
                            break;
                        case "idorpassport":
                            filteredQuery = filteredQuery.OrderBy(s => s.idOrPassport);
                            break;
                        case "genders":
                            filteredQuery = filteredQuery.OrderBy(s => s.Gender);
                            break;
                        case "telephone":
                            filteredQuery = filteredQuery.OrderBy(s => s.Telephone);
                            break;
                        case "emailaddress":
                            filteredQuery = filteredQuery.OrderBy(s => s.Email);
                            break;
                        case "addressLine1":
                            filteredQuery = filteredQuery.OrderBy(s => s.AddressLine1);
                            break;
                        case "addressLine2":
                            filteredQuery = filteredQuery.OrderBy(s => s.AddressLine2);
                            break;
                        case "city":
                            filteredQuery = filteredQuery.OrderBy(s => s.City);
                            break;
                        case "postalCode":
                            filteredQuery = filteredQuery.OrderBy(s => s.PostalCode);
                            break;

                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "username":
                            filteredQuery = filteredQuery.OrderByDescending(s => s.UserName);
                            break;
                        case "title":
                            filteredQuery = filteredQuery.OrderByDescending(s => s.Title);
                            break;
                        case "firstname":
                            filteredQuery = filteredQuery.OrderByDescending(s => s.FirstName);
                            break;
                        case "surname":
                            filteredQuery = filteredQuery.OrderByDescending(s => s.Surname);
                            break;
                        case "idorpassport":
                            filteredQuery = filteredQuery.OrderByDescending(s => s.idOrPassport);
                            break;
                        case "genders":
                            filteredQuery = filteredQuery.OrderByDescending(s => s.Gender);
                            break;
                        case "telephone":
                            filteredQuery = filteredQuery.OrderByDescending(s => s.Telephone);
                            break;
                        case "emailaddress":
                            filteredQuery = filteredQuery.OrderByDescending(s => s.Email);
                            break;
                        case "addressLine1":
                            filteredQuery = filteredQuery.OrderByDescending(s => s.AddressLine1);
                            break;
                        case "addressLine2":
                            filteredQuery = filteredQuery.OrderByDescending(s => s.AddressLine2);
                            break;
                        case "city":
                            filteredQuery = filteredQuery.OrderByDescending(s => s.City);
                            break;
                        case "postalCode":
                            filteredQuery = filteredQuery.OrderByDescending(s => s.PostalCode);
                            break;

                    }
                    break;
            }

            //setup paging
            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);

            return SerializeToAngular(new GridResultModel<AssessorGridModel>(filteredQuery.ToList(), totalrecords));
        }
        public ActionResult AssessorGet(long? id)
        {
            if (id == null)
                id = 0;

            try
            {
                //do some work here on the provider 
                var model = assessorProvider.GetAssessor().Where(a => a.Id == id).Select(a => new AssessorModel()
                {
                    Id = a.Id,
                    UserIdentityId = a.UserIdentityId,
                    UserName = a.UserIdentities.UserName,
                    Password = a.UserIdentities.PasswordHash,
                    Email = a.UserIdentities.EmailAddress,
                    Title = a.UserIdentities.Title,
                    FirstName = a.UserIdentities.FirstName,
                    Surname = a.UserIdentities.Surname,
                    IDOrPassport = a.UserIdentities.IdPassportNum,
                    GenderType = a.UserIdentities.Gender,
                    Telephone = a.UserIdentities.Telephone,
                    AddressLine1 = a.UserIdentities.AddressLine1,
                    AddressLine2 = a.UserIdentities.AddressLine2,
                    City = a.UserIdentities.City,
                    PostalCode = a.UserIdentities.PostalCode,

                }).Single();
                return SerializeToAngular(model);
            }
            catch (AssessorException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }
        
    }
}