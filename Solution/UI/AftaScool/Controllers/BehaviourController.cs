﻿using AftaScool.BL.Context;
using AftaScool.BL.Provider.BehaviourData;
using AftaScool.Models;
using AftaScool.Models.Behaviour;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace AftaScool.Controllers
{
    [Authorize]
    public class BehaviourController : TCRControllerBase
    {
        #region Ctor
        public IBehaviourProvider BehaviourProvider { get; set; }
        public DataContext DataContext { get; set; }
        private bool _MustDisposeContext = true;

        public BehaviourController()
        {
            DataContext = new DataContext();
            BehaviourProvider = new BehaviourProvider(DataContext, CurrentUser);

        }
        public BehaviourController(IBehaviourProvider behaviourProvider)
        {
            _MustDisposeContext = false;
            BehaviourProvider = behaviourProvider;
        }
        protected override void Dispose(bool disposing)
        {
            if (_MustDisposeContext)
                DataContext.Dispose();
            base.Dispose(disposing);
        }

        #endregion


        [HttpPost]
        public ActionResult SaveBehaviour(BehaviourModel model)
        {
            try
            {
                //returns a condition based on the request.
                if (!ModelState.IsValid)
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error details");

                //SaveBehaviour
                var behave = BehaviourProvider.saveBehaviour(model.Id, model.Type);
                model.Id = behave.Id;
                return SerializeToAngular(model);
            }
            catch (BehaviourException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }
        public ActionResult BehaviourList()
        {
            var behaviours = BehaviourProvider.GetBehaviourTypes().Where(a => a.Id == a.Id)
                            .Select(b => new KeyValueModel { Id = b.Id, Description = b.Type }).OrderBy(b=>b.Description).ToList();

            return SerializeToAngular(behaviours);
        }
        public ActionResult GetBehaviour(long? id)
        {
            if (id == null)
                id = 0;

            try
            {
                //do some work here on the provider 
                var model = BehaviourProvider.GetBehaviourTypes().Where(a => a.Id==id).Select(a => new BehaviourModel()
                 {
                    Id = a.Id,
                    Type = a.Type

                }).Single();
                return SerializeToAngular(model);
            }
            catch (BehaviourException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}