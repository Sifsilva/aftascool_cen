﻿using AftaScool.BL.Context;
using AftaScool.BL.Provider.QuestionnaireData;
using AftaScool.Models;
using AftaScool.Models.Questionnaire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace AftaScool.Controllers
{
   // [Authorize]
    public class QuestionnaireController : TCRControllerBase
    {
        

        #region Ctor

        public IQuestionnaireProvider QuestionnaireProvider { get; set; }
        public DataContext DataContext { get; set; }
        private bool _MustDisposeContext = true;

        public QuestionnaireController()
        {
            DataContext = new DataContext();
            QuestionnaireProvider = new QuestionnaireProvider(DataContext, CurrentUser);
        }

        public QuestionnaireController(IQuestionnaireProvider questionnaireProvider)
        {
            _MustDisposeContext = false;
            QuestionnaireProvider = questionnaireProvider;
        }


        protected override void Dispose(bool disposing)
        {
            if (_MustDisposeContext)
                DataContext.Dispose();
            base.Dispose(disposing);
        }


        #endregion

        [HttpPost]
        public ActionResult QuestionnaireSave(QuestionnaireModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error details");

                 var questionnaire = QuestionnaireProvider.saveQuestionnaire(model.Id, model.AssessorId, model.LearnerId, model.QuestionnaireDate);
                model.Id= questionnaire.Id;
                return SerializeToAngular(model);

            }
            catch (QuestionnaireException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }

        }

       public ActionResult QuestionnaireList()
        {
            var itms = QuestionnaireProvider.GetQuestionnaires()
                                    .Where(a => a.Id == a.Id)
                                    .Select(b => new KeyValueModel { Id = b.Id, Description = b.LearnerId.ToString() })
                                    .OrderBy(b => b.Description)
                                    .ToList();
            return SerializeToAngular(itms);
        }
        
        /*
        [HttpPut]



        public ActionResult QuestionnaireGet(long? id)
        {
            if (id == null)
                id = 0;
            try
            {
                //do some work here on the provider
                var model = QuestionnaireProvider.GetQuestionnaires().Where(a => a.Id == id).Select(a => new QuestionnaireModel()
                {
                    Id = a.Id,
                    AssessorId = a.AssessorId,
                    LearnerId = a.LearnerId,
                    QuestionnaireDate = a.QuestionnaireDate,
                }).Single();
                return SerializeToAngular(model);
            }
            catch (QuestionnaireException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }*/
    }

}
