﻿using AftaScool.BL.Context;
using AftaScool.BL.Entities.LearnerData;
using AftaScool.BL.Provider.BehaviourData;
using AftaScool.Models;
using AftaScool.Models.Behaviour;
using AftaScool.Models.BehaviourQuestion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace AftaScool.Controllers
{
    //[Authorize]
    public class BehaviourQuestionController : TCRControllerBase
    {
        #region Ctor
        public IBehaviourQuestionProvider BehaviourQuestionProvider { get; set; }
        public DataContext DataContext { get; set; }
        private bool _MustDisposeContext = true;

        public BehaviourQuestionController()
        {
            DataContext = new DataContext();
            BehaviourQuestionProvider = new BehaviourQuestionProvider(DataContext, CurrentUser);

        }
        public BehaviourQuestionController(IBehaviourQuestionProvider behaviourQuestionProvider)
        {
            _MustDisposeContext = false;
            BehaviourQuestionProvider = behaviourQuestionProvider;
        }
        protected override void Dispose(bool disposing)
        {
            if (_MustDisposeContext)
                DataContext.Dispose();
            base.Dispose(disposing);
        }

        #endregion

        [HttpPost]
        public ActionResult behaviourQuestionSave(BehaviourQuestionModel model)
        {
            try
            {
                //returns a condition based on the request.
                if (!ModelState.IsValid)
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error details");

                //SaveBehaviourQuesiton
                var behavequestion = BehaviourQuestionProvider.bquestion(model.Id, model.BehaviourId, model.MinimumWeight, model.MaximumWeight);
                model.Id = behavequestion.Id;
                return SerializeToAngular(model);
            }
            catch (BehaviourException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }
        public ActionResult BehaviourQuestionList()
        {
            
            var itms = BehaviourQuestionProvider.GetBehaviours()
                                     .Where(a => a.Id==a.Id)
                                     .Select(b => new KeyValueModel { Id = b.Id, Description = b.BehaviourId.ToString()})
                                     .OrderBy(b => b.Description)
                                     .ToList();
            return SerializeToAngular(itms);
        }
    }
}