﻿using AftaScool.BL.Context;
using AftaScool.BL.Provider.SchoolData;
using AftaScool.Models;
using AftaScool.Models.School;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace AftaScool.Controllers
{
    [Authorize]
    public class SchoolController : TCRControllerBase
    {
        // GET: School

        #region Ctor

        public ISchoolProvider SchoolProvider { get; set; }
        public DataContext DataContext { get; set; }
        private bool _MustDisposeContext = true;

        public SchoolController()
        {
            DataContext = new DataContext();
            SchoolProvider = new SchoolProvider(DataContext, CurrentUser);
        }

        public SchoolController(ISchoolProvider schoolProvider)
        {
            _MustDisposeContext = false;
            SchoolProvider = schoolProvider;
        }


        protected override void Dispose(bool disposing)
        {
            if (_MustDisposeContext)
                DataContext.Dispose();
            base.Dispose(disposing);
        }


        #endregion

        [HttpPost]
        public ActionResult SchoolSave(SchoolModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error details");

                var school = SchoolProvider.SchoolSave(model.Id, model.SchoolName,model.SchoolType, model.AddressLine1, model.AddressLine2, model.City, model.PostalCode,model.telephone,model.Location);
                model.Id = school.Id;
                return SerializeToAngular(model);

            }
            catch (SchoolException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }

        }

        public ActionResult SchoolList()
        {
            var itms = SchoolProvider.GetSchools()
                                    .Where(a => a.Schoool == BL.Entities.SchoolData.SchoolType.HighSchool || a.Schoool == BL.Entities.SchoolData.SchoolType.PrePrimary || a.Schoool == BL.Entities.SchoolData.SchoolType.Primary)
                                    .Select(b => new KeyValueModel { Id = b.Id, Description = b.SchoolName })
                                    .OrderBy(b => b.Description)
                                    .ToList();
            return SerializeToAngular(itms);
        }
        [HttpPut]

        public ActionResult SchoolGrid(GridModel model)
        {

            int begin = SetupGridParams(model);

            var filteredQuery = SchoolProvider.GetSchools()
                                                        .Select(a => new SchoolGridModel()
                                                        {
                                                            Id = a.Id,
                                                            SchoolName = a.SchoolName,
                                                            SchoolType = a.Schoool,
                                                            City = a.City,
                                                            AddressLine1 = a.AddressLine1,
                                                            AddressLine2 = a.AddressLine2,
                                                            PostalCode = a.PostalCode,
                                                            Telephone = a.Telephone,
                                                            Location = a.Location
                                                        });
            if (model.Searchfor != "null")
            {
                filteredQuery = filteredQuery.Where(r => r.SchoolName.Contains(model.Searchfor)
                                                    || r.AddressLine1.Contains(model.Searchfor)
                                                    || r.AddressLine2.Contains(model.Searchfor)
                                                    || r.City.Contains(model.Searchfor)
                                                    || r.PostalCode.Contains(model.Searchfor)
                                                    || r.Telephone.Contains(model.Searchfor)
                                                    );
            }
            //Get Reord count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.SchoolName); //default sort order

            //Setup sort order
            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "schoolname":
                            filteredQuery = filteredQuery.OrderBy(r => r.SchoolName);
                            break;
                        case "status":
                            filteredQuery = filteredQuery.OrderBy(r => r.SchoolType);
                            break;
                        case "addressline1":
                            filteredQuery = filteredQuery.OrderBy(r => r.AddressLine1);
                            break;
                        case "addressline2":
                            filteredQuery = filteredQuery.OrderBy(r => r.AddressLine2);
                            break;
                        case "city":
                            filteredQuery = filteredQuery.OrderBy(r => r.City);
                            break;
                        case "postalcode":
                            filteredQuery = filteredQuery.OrderBy(r => r.PostalCode);
                            break;
                        case "Telephone":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Telephone);
                            break;
                        case "location":
                            filteredQuery = filteredQuery.OrderBy(r => r.Location);
                            break;

                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "schoolname":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.SchoolName);
                            break;
                        case "status":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.SchoolType);
                            break;
                      
                        case "addressline1":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.AddressLine1);
                            break;
                        case "addressline2":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.AddressLine2);
                            break;
                        case "city":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.City);
                            break;
                        case "postalcode":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.PostalCode);
                            break;
                        case "Telephone":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Telephone);
                            break;
                        case "location":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Location);
                            break;
                    }
                    break;
            }

            //setup paging
            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);


            return SerializeToAngular(new GridResultModel<SchoolGridModel>(filteredQuery.ToList(), totalNumberOfRecords));
        }

        public ActionResult SchoolGet(long? id)
        {
            if (id == null)
                id = 0;
            try
            {
                
                var model = SchoolProvider.GetSchools().Where(a => a.Id == id).Select(a => new SchoolModel()
                {
                    Id = a.Id,
                    SchoolName = a.SchoolName,
                    SchoolType= a.Schoool,
                    City = a.City,
                    AddressLine1 = a.AddressLine1,
                    AddressLine2 = a.AddressLine2,
                    PostalCode = a.PostalCode,
                    telephone= a.Telephone,
                    Location = a.Location
                }).Single();
                return SerializeToAngular(model);
            }
            catch (SchoolException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }

}