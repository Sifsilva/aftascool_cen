﻿using AftaScool.BL.Context;
using AftaScool.BL.Provider.LearnerData;
using AftaScool.Models;
using AftaScool.Models.Learner;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AftaScool.BL.Entities.LearnerData;
using AftaScool.BL.Provider.AssessorData;


namespace AftaScool.Controllers
{
    [Authorize]
    public class LearnerAssessorController:TCRControllerBase
    {
        #region Ctor
        
        public ILearnerAssessor LearnerAssessorProvider { get; set; }
        public DataContext DataContext { get; set; }
        private bool _MustDisposeContext = true;

        public LearnerAssessorController()
        {
            DataContext = new DataContext();
            LearnerAssessorProvider  = new LearnerAssessorProvider(DataContext, CurrentUser);
        }

        public LearnerAssessorController(ILearnerAssessor learnerAssessorProvider)
        {
            _MustDisposeContext = false;
            LearnerAssessorProvider = learnerAssessorProvider;
        }

        protected override void Dispose(bool disposing)
        {
            if (_MustDisposeContext)
                DataContext.Dispose();
            base.Dispose(disposing);
        }

        #endregion

        public ActionResult saveLearnerAssessor(LearnerAssessorModel model)
        {
            try
            {
                //returns a condition based on the request.
                if (!ModelState.IsValid)
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error details");

                var learnerAssessor = LearnerAssessorProvider.SaveLearnerAssessor(model.Id,model.AssessorId,model.LearnerId);

                model.Id = learnerAssessor.Id;

                return SerializeToAngular(model);
            }
            catch (LearnerException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }

        }
        public ActionResult LearnerGet(long? id)
        {
            if (id == null)
                id = 0;

            try
            {

                var model = LearnerAssessorProvider.GetLearnerAssessor().Where(a => a.Id == id).Select(a => new LearnerAssessorModel()
                {
                    Id = a.Id,
                    AssessorId = a.AssessorId,
                    LearnerId = a.LearnerId
                }).Single();
                return SerializeToAngular(model);
            }
            catch (LearnerException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}