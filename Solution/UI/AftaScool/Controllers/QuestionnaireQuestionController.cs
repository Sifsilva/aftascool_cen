﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AftaScool.BL.Provider.QuestionnaireData;
using System.Web.Mvc;
using AftaScool.Models.QuestionnaireQuestion;
using System.Net;
using AftaScool.BL.Context;
using AftaScool.Models;

namespace AftaScool.Controllers
{
    public class QuestionnaireQuestionController : TCRControllerBase
    {
        // GET: Questionnaire

        #region Ctor

        public IQuestionnaireQuestionProvider QuestionnaireQuestionProvider { get; set; }
        public DataContext DataContext { get; set; }
        private bool _MustDisposeContext = true;

        public QuestionnaireQuestionController()
        {
            DataContext = new DataContext();
            QuestionnaireQuestionProvider = new QuestionnaireQuestionProvider(DataContext, CurrentUser);
        }

        public QuestionnaireQuestionController(IQuestionnaireQuestionProvider questionnaireProvider)
        {
            _MustDisposeContext = false;
            QuestionnaireQuestionProvider = QuestionnaireQuestionProvider;
        }


        protected override void Dispose(bool disposing)
        {
            if (_MustDisposeContext)
                DataContext.Dispose();
            base.Dispose(disposing);
        }


        #endregion
        public ActionResult QuestionnaireQuestionSave(QuestionnaireQuestionModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error details");

                var questionnairequestion = QuestionnaireQuestionProvider.saveQuestion(model.Id, model.QuestionnaireId, model.BehaviourQuestionId, model.Trait);
                model.Id = questionnairequestion.Id;
                return SerializeToAngular(model);

            }
            catch (QuestionnaireException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }

        }

        public ActionResult QuestionnaireQuestionList()
        {
            var itms = QuestionnaireQuestionProvider.getQuestions()
                                    .Where(a => a.Id == a.Id)
                                    .Select(b => new KeyValueModel { Id = b.Id })
                                    .OrderBy(b => b.Description)
                                    .ToList();
            return SerializeToAngular(itms);
        }
        [HttpPut]

        public ActionResult QuestionnaireQuestionGet(long? id)
        {
            if (id == null)
                id = 0;
            try
            {
                //do some work here on the provider
                var model = QuestionnaireQuestionProvider.getQuestions().Where(a => a.Id == id).Select(a => new QuestionnaireQuestionModel()
                {
                    Id = a.Id,
                    QuestionnaireId = a.QuestionnaireId,
                    BehaviourQuestionId = a.BehaviourQuestionId,
                    Trait = a.Trait,
                }).Single();
                return SerializeToAngular(model);
            }
            catch (QuestionnaireException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}