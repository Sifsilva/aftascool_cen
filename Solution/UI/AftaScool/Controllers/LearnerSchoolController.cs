﻿using AftaScool.BL.Context;
using AftaScool.BL.Entities.LearnerData;
using AftaScool.BL.Provider.LearnerData;
using AftaScool.Models.LearnerSchool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace AftaScool.Controllers
{
    [Authorize]
    public class LearnerSchoolController : TCRControllerBase
    {
        #region Ctor
        public ILearnerSchoolProvider LearnerSchoolProvider { get; set; }
        public DataContext DataContext { get; set; }
        private bool _MustDisposeContext = true;

        public LearnerSchoolController()
        {

            DataContext = new DataContext();
            LearnerSchoolProvider = new LearnerSchoolProvider(DataContext, CurrentUser);

        }
        public LearnerSchoolController(ILearnerSchoolProvider learnerSchoolProvider)
        {
            _MustDisposeContext = false;
            LearnerSchoolProvider = learnerSchoolProvider;
        }
        protected override void Dispose(bool disposing)
        {
            if (_MustDisposeContext)
                DataContext.Dispose();
            base.Dispose(disposing);
        }

        #endregion

        [HttpPost]
        public ActionResult saveLearnerSchool(LearnerSchoolModel model)
        {
                  
            try
            {
                
                
                if (!ModelState.IsValid)
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error details");

                //SaveLeanerSchool
                var learner = LearnerSchoolProvider.saveLearnerSchool(model.Id, model.LearnerId, model.SchoolId, model.StartDate, model.EndDate);
                model.Id = learner.Id;
                return SerializeToAngular(model);
            }
            catch (LearnerSchoolException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}
