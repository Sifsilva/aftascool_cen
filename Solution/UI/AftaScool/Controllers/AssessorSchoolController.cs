﻿using AftaScool.BL.Context;
using AftaScool.BL.Provider.AssessorData;
using AftaScool.Models;
using AftaScool.Models.AssessorSchool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace AftaScool.Controllers
{
    [Authorize]
    public class AssessorSchoolController : TCRControllerBase
    {
        #region Ctor

        public IAssessorSchoolProvider AssessorSchoolProvider { get; set; }
        public DataContext DataContext { get; set; }
        private bool _MustDisposeContext = true;

        public AssessorSchoolController()
        {
            DataContext = new DataContext();
            AssessorSchoolProvider = new AssessorSchoolProvider(DataContext, CurrentUser);
        }
        protected override void Dispose(bool disposing)
        {
            if (_MustDisposeContext)
                DataContext.Dispose();
            base.Dispose(disposing);
        }
        #endregion
        [HttpPost]
        public ActionResult saveAssessorSchool(AssessorSchoolModel model)
        {
            try
            {
                //returns a condition based on the request.
                if (!ModelState.IsValid)
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error details");
                //ido some work here on the provider
                var assessorSchool = AssessorSchoolProvider.saveAssessorSchool(model.Id, model.AssessorId, model.SchoolId, model.StartDate, model.EndDate);
                model.Id = assessorSchool.Id;
                return SerializeToAngular(model);
            }
            catch (AssessorSchoolException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }
        public ActionResult AssessorSchoolList()
        {
            var itms = AssessorSchoolProvider.GetAssessorSchool()
                                     .Where(a => a.Id==a.Id)
                                     .Select(b => new KeyValueModel { Id = b.Id})
                                     .OrderBy(b => b.Description)
                                     .ToList();
            return SerializeToAngular(itms);
        }
        public ActionResult GetAssessorSchool(long? id)
        {
            if (id == null)
                id = 0;

            try
            {
                //do some work here on the provider
                var model = AssessorSchoolProvider.GetAssessorSchool().Where(a => a.Id == id).Select(a => new AssessorSchoolModel()
                {
                    Id = a.Id,
                    AssessorId = a.AssessorId,
                    SchoolId = a.SchoolId,
                    StartDate = a.StartDate,
                    EndDate = a.EndDate,
                    

                }).SingleOrDefault();
                return SerializeToAngular(model);
            }
            catch (AssessorSchoolException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }

    }
}