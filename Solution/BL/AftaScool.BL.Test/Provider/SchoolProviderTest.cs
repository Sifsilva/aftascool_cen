﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftwareApproach.TestingExtensions;
using AftaScool.BL.Entities.SchoolData;
using AftaScool.BL.Provider.SchoolData;
using AftaScool.BL.Test;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AftaScool.BL.Test.Provider
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class SchoolProviderTest:ProviderTestBase
    {
        [TestMethod]
        [TestCategory("Provider.School")]
        public void SchoolSave()
        {
            //SetUp
            var user = SeedData.CreateAdminUser(Context);
            ISchoolProvider provider = new SchoolProvider(Context,user);
            
            //Act
            var school = provider.SchoolSave(null, "Carel De Vet HighSchool", Entities.SchoolData.SchoolType.HighSchool, "145 Gamyn Ave", "North", "Vereniging", "0001","0726568841", null);
            
            //Test
            school.Id.ShouldBeGreaterThan(0);
            school.ShouldNotBeNull();
        }
        [TestMethod]
        [TestCategory("Provider.School")]
        public void SchoolList()
        {
            //SetUp
            var user = SeedData.CreateAdminUser(Context);
            ISchoolProvider provider = new SchoolProvider(Context,user);
            var school = provider.SchoolSave(null, "Carel De HighSchool", Entities.SchoolData.SchoolType.HighSchool, "145 Gamyn Ave", "North1", "Vereniging", "0001", "0726568841", null);
            var school1 = provider.SchoolSave(null, "Carel De Vet ", Entities.SchoolData.SchoolType.Primary, "145 Gamyn Ave", "North", "Vereniging", "0001", "0726568841", null);
            var school2 = provider.SchoolSave(null, "Vet HighSchool", Entities.SchoolData.SchoolType.HighSchool, "145 Gamynd Ave", "Ndorth", "Verenigdfing", "0001", "0726568841", null);

            //Act
            var chkSchool = provider.GetSchools().Count();

            //Test
            chkSchool.ShouldEqual(3);
            
        }
        [TestMethod]
        [TestCategory("Provider.School")]
        public void ArchiveSchool()
        {
            //SetUp
            var user = SeedData.CreateAdminUser(Context);
            ISchoolProvider provider = new SchoolProvider(Context, user);
            var school = provider.SchoolSave(null, "Carel De Vet HighSchool", Entities.SchoolData.SchoolType.HighSchool, "145 Gamyn Ave", "North", "Vereniging", "0001", "0726568841", null);

            //Act
            provider.ArchiveSchool(user.Id);

            //Test
            var testSchool = provider.GetSchools().Where(a => a.Id == school.Id).Single();
        }


    }
}
