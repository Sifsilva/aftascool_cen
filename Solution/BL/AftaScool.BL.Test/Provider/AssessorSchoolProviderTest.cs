﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SoftwareApproach.TestingExtensions;
using AftaScool.BL.Test;
using AftaScool.BL.Entities.AssessorData;
using System.Diagnostics.CodeAnalysis;
using AftaScool.BL.Provider.LearnerData;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AftaScool.BL.Provider.AssessorData;
using AftaScool.BL.Provider.SchoolData;

namespace AftaScool.BL.Test.Provider
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class AssessorSchoolProviderTest : ProviderTestBase
    {
        [TestMethod]
        [TestCategory("Provider.AssessorSchool")]
        public void saveAssessorSchool()
        {
            //SetUp
            var user = SeedData.CreateAdminUser(Context);
            IAssessorSchoolProvider provider = new AssessorSchoolProvider(Context,user);
            ISchoolProvider skul = new SchoolProvider(Context, user);
            IAssessorProvider _assesor = new AssessorProvider(Context, user);
            var assess = _assesor.SaveAssessor(null, "Test UserName12", "Test password12", "Test email", "Test Title", "Test FirstName", "Test Surname", "92022802168083", Entities.SecurityData.GenderType.Female, "0124566512", "address line1", "address line 2", "Centurion", "0124");
            var skuul = skul.SchoolSave(null, "Carel De Vet HighSchool", Entities.SchoolData.SchoolType.HighSchool, "145 Gamyn Ave", "North", "Vereniging", "0001", "0725505506",null);
            

            //Act
            var assessorSchool = provider.saveAssessorSchool(null, assess.Id,skuul.Id,DateTime.Now,DateTime.Today.AddDays(6));
          
            //Test
            assessorSchool.ShouldNotBeNull();
            assessorSchool.Id.ShouldNotBeNull();
        
        }

        [TestMethod]
        [TestCategory("Provider.Learner")]
        public void assessorSchoolListTest()
        {
            var user = SeedData.CreateAdminUser(Context);
            IAssessorSchoolProvider provider = new AssessorSchoolProvider(Context, user);
            IAssessorProvider _assesor = new AssessorProvider(Context, user);
            ISchoolProvider skul = new SchoolProvider(Context, user);
            var assess = _assesor.SaveAssessor(null, "Test UserName12", "Test password12", "Test email", "Test Title", "Test FirstName", "Test Surname", "92022802168083", Entities.SecurityData.GenderType.Female, "0124566512", "address line1", "address line 2", "Centurion", "0124");
            var skuul = skul.SchoolSave(null, "Carel De Vl", Entities.SchoolData.SchoolType.HighSchool, "145 Gamyn Ave", "North", "Vereniging", "0001","07254648879", null);
            var skuul2 = skul.SchoolSave(null, "Carel De Vet", Entities.SchoolData.SchoolType.HighSchool, "145 Gamyn Ave", "North", "Vereniging", "0001", "07254648879", null);

            var assessorSchool = provider.saveAssessorSchool(null, assess.Id, skuul.Id, DateTime.Now, DateTime.Today);
            var assessorSchool2 = provider.saveAssessorSchool(null, assess.Id, skuul2.Id, DateTime.Now, DateTime.Today);

            //Act
            var schools = provider.GetAssessorSchool().Count();

            //Test
            schools.ShouldEqual(2);
        }



    }
}