﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AftaScool.BL.Context;
using AftaScool.BL.Test;
using AftaScool.BL.Entities.SecurityData;

namespace AftaScool.BL.Test
{
    /*
     * This class contains 1 running test method.
     * The execution of this method will be slow as it attempts to create and seed a database with allot of data in it
     * this data can be used for report development.
     * 
     * I have commented out the [TestMethod] attribute for quick testing.
     
     */
    [TestClass]
    public class ContextTest : ProviderTestBase
    {
        [TestMethod]
        [TestCategory("Utilities")]
        public void TestCreateDB()
        {
            DataContext.Setup();
        }

      //  [TestMethod]
        [TestCategory("Utilities")]
        public void SeedRandomDB()
        {
            using (var ctx = new DataContext())
            {
               
            }
        }

      
    }
}
