﻿using AftaScool.BL.Entities.SchoolData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AftaScool.BL.Entities.AssessorData
{
    [Table ("AssessorSchool")]
    public class AssessorSchool
    {
        //Please include database indexes on this entity to ease query
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        public virtual long AssessorId { get; set; }
        [ForeignKey("AssessorId")]

        public virtual Assessor Assessor { get; set; }

        public virtual long SchoolId { get; set; }
        [ForeignKey("SchoolId")]

        public virtual School School { get; set; }

       
        [Display(Name = "Start Date")]
        [Required]
        [DataType(DataType.Text)]
        [DisplayFormat(DataFormatString = "{yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public virtual DateTime StartDate { get; set; }


        [Display(Name = "End Date")]
        [Required]
        [DataType(DataType.Text)]
        [DisplayFormat(DataFormatString = "{yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public virtual DateTime EndDate { get; set; }

        


    }
}