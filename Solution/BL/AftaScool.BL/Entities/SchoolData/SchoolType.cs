﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AftaScool.BL.Entities.SchoolData
{
    public enum SchoolType
    {
        PrePrimary,
        Primary,
        HighSchool

    }
}