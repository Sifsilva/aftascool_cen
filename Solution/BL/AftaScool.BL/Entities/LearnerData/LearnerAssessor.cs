﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AftaScool.BL.Entities.AssessorData;

namespace AftaScool.BL.Entities.LearnerData
{
    [Table("LearnerAssessor")]
    public class LearnerAssessor
    {

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        
        public virtual long AssessorId { get; set; }
        [ForeignKey("AssessorId")]
        public virtual Assessor Assessors { get; set; }

        public virtual long LearnerId { get; set; }
        [ForeignKey("LearnerId")]

        public virtual Learner Learners { get; set; }
    }
}