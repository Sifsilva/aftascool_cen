﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AftaScool.BL.Types
{
    public enum StatusType
    {
        Active,
        Inactive,
        Archive
    }
}