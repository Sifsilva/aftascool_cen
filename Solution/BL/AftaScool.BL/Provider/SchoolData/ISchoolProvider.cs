﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AftaScool.BL.Entities.SchoolData;
using System.Data.Entity.Spatial;


namespace AftaScool.BL.Provider.SchoolData
{
    public interface ISchoolProvider:IAftaScoolProvider
    {

        School SchoolSave(long? id, string schoolName, SchoolType schooltype, string addressline1, string addressline2, string city, string postalCode, string telephone, DbGeography location);
        
        void ArchiveSchool(long id);

        IQueryable<School> GetSchools();

       
    }
}
