﻿using AftaScool.BL.Context;
using AftaScool.BL.Entities.AssessorData;
using AftaScool.BL.Entities.SecurityData;
using AftaScool.BL.Provider.Security;
using AftaScool.BL.Types;
using AftaScool.BL.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AftaScool.BL.Provider.AssessorData
{
    public class AssessorProvider : AftaScoolProvider, IAssessorProvider
    {
        #region Ctor

        public AssessorProvider(DataContext context, ICurrentUser currentUser)
            : base(context, currentUser)
        { }
        #endregion

        #region Save Assessor Implementation

        public Assessor SaveAssessor(long? id, string username, string password, string emailAddress, string title, string firstname, string surname, string idOrPassportNumber, GenderType gender,
            string telephone, string addressLine1, string addressLine2, string city, string postalCode)
        {

            Authenticate(PrivilegeType.AssessorMaintenance);

            UserIdentity user1 = new UserIdentity();
            Assessor createAssessor = new Assessor();


            createAssessor = DataContext.AssessorSet.Where(p => p.UserIdentities.UserName == username && p.Id != id).SingleOrDefault();

            if (createAssessor != null)
            {
                throw new AssessorException("This assessor already exist.");
            }

            if (id != null && id > 0)
            {
                createAssessor = DataContext.AssessorSet.Where(p => p.Id == id).SingleOrDefault();

            }


            var user = new UserIdentity()
            {

                UserName = username,
                PasswordHash = Cipher.Encrypt(password),
                EmailAddress = emailAddress,
                Title = title,
                FirstName = firstname,
                Surname = surname,
                IdPassportNum = idOrPassportNumber,
                Gender = gender,
                Telephone = telephone,
                AddressLine1 = addressLine1,
                AddressLine2 = addressLine2,
                City = city,
                PostalCode = postalCode,
                Active = true,

            };
            if (id != null)
            {
                user1 = DataContext.UserIdentitySet.Where(a => a.Id == createAssessor.UserIdentityId).SingleOrDefault();

                user1.UserName = username;
                user1.PasswordHash = Cipher.Encrypt(password);
                user1.EmailAddress = emailAddress;
                user1.Title = title;
                user1.FirstName = firstname;
                user1.Surname = surname;
                user1.IdPassportNum = idOrPassportNumber;
                user1.Gender = gender;
                user1.Telephone = telephone;
                user1.AddressLine1 = addressLine1;
                user1.AddressLine2 = addressLine2;
                user1.City = city;
                user1.PostalCode = postalCode;
                user1.Active = true;

                DataContextSaveChanges();


            }

            if (id == null)
                DataContext.UserIdentitySet.Add(user);
            DataContextSaveChanges();



            createAssessor = new Assessor();
            createAssessor.UserIdentities = DataContext.UserIdentitySet.Where(d => d.Id == user.Id).SingleOrDefault();

            if (id == null)
                DataContext.AssessorSet.Add(createAssessor);
            DataContextSaveChanges();




            return createAssessor;

        }
        #endregion
        public IQueryable<Assessor> GetAssessor()
        {

            var q = from h in DataContext.AssessorSet
                    orderby h.UserIdentities.FirstName
                    select h;

            return q;

        }

        public void GetAssessor(long id)
        {
            Authenticate(PrivilegeType.AssessorMaintenance);

            var assessor = DataContext.AssessorSet.Where(a => a.Id == id).Single();
            DataContextSaveChanges();

        }


    }
}