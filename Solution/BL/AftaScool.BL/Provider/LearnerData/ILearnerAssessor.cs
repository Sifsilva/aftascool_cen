﻿using AftaScool.BL.Entities.LearnerData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AftaScool.BL.Provider.AssessorData
{
  public interface ILearnerAssessor:IAftaScoolProvider
    {
        #region method definition
        LearnerAssessor SaveLearnerAssessor(long? id, long assessorId, long learnerId);

        IQueryable<LearnerAssessor> GetLearnerAssessor();
        #endregion

    }
}
