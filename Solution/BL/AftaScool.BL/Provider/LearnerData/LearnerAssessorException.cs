﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AftaScool.BL.Provider.AssessorData
{
    public class LearnerAssessorException:Exception
    {

         public LearnerAssessorException(string errorMessage) :base(errorMessage){ }
    }
}