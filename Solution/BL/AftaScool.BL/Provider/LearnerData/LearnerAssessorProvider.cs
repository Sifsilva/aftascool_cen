﻿
using AftaScool.BL.Context;
using AftaScool.BL.Entities.LearnerData;
using AftaScool.BL.Entities.SecurityData;
using AftaScool.BL.Provider.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AftaScool.BL.Provider.AssessorData
{
    public class LearnerAssessorProvider : AftaScoolProvider, ILearnerAssessor
    {
        #region Ctor


        public LearnerAssessorProvider(DataContext context, ICurrentUser currentUser)
            : base(context, currentUser)
        { }
        public LearnerAssessorProvider(DataContext context)
            : this(context,null)
        { }
        #endregion 

        #region Save Assessor Implementation

        public LearnerAssessor SaveLearnerAssessor(long? id, long assessorId, long learnerId)
        {
             

            Authenticate(PrivilegeType.LearnerAssessorMaintenance);

            LearnerAssessor  saveLearnerAssessor = new LearnerAssessor();
                
            if (id != null && id > 0)
                 saveLearnerAssessor = DataContext.LearnerAssessorSet.Where(p => p.Id == id && p.AssessorId == assessorId && p.LearnerId == learnerId).SingleOrDefault();

            else
            {
                saveLearnerAssessor = new LearnerAssessor();
                DataContext.LearnerAssessorSet.Add(saveLearnerAssessor);
            }

          
            saveLearnerAssessor.AssessorId = assessorId;
            saveLearnerAssessor.LearnerId = learnerId;

            DataContextSaveChanges();
            return saveLearnerAssessor;



        }
        
        #endregion
        #region glA

        public IQueryable<LearnerAssessor> GetLearnerAssessor()
        {
            var q = from h in DataContext.LearnerAssessorSet
                    orderby h.Id
                    select h;

            return q;
        }

        #endregion

    }

    
}